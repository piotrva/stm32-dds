/*********************************************************************
*                SEGGER Microcontroller GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 1996 - 2014  SEGGER Microcontroller GmbH & Co. KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

** emWin V5.24 - Graphical user interface for embedded applications **
All  Intellectual Property rights  in the Software belongs to  SEGGER.
emWin is protected by  international copyright laws.  Knowledge of the
source code may not be used to write a similar product.  This file may
only be used in accordance with the following terms:

The software has been licensed to  ARM LIMITED whose registered office
is situated at  110 Fulbourn Road,  Cambridge CB1 9NJ,  England solely
for  the  purposes  of  creating  libraries  for  ARM7, ARM9, Cortex-M
series,  and   Cortex-R4   processor-based  devices,  sublicensed  and
distributed as part of the  MDK-ARM  Professional  under the terms and
conditions  of  the   End  User  License  supplied  with  the  MDK-ARM
Professional. 
Full source code is available at: www.segger.com

We appreciate your understanding and fairness.
----------------------------------------------------------------------
File        : main.c
Purpose     : Main program
---------------------------END-OF-HEADER------------------------------
*/

#include "stm32f4xx_hal.h"
#include "RTE_Components.h"
#include "GUI.h"
#include "BUTTON.h"
#include "FRAMEWIN.h"
#include "TEXT.h"
#include "IMAGE.h"
#include "DIALOG.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "System.h"
#include "dac_msp.h"

#ifdef RTE_CMSIS_RTOS_RTX
extern uint32_t os_time;

uint32_t HAL_GetTick(void)
{
	return os_time;
}
#endif

uint32_t DialogElementCount = 0;
uint32_t guiElementCount = 0;

GUI_WIDGET_CREATE_INFO DialogCreateList[20];

typedef struct {
	int x0;
	int y0;
	int xSize;
	int ySize;
	char *text;
	void (*clickCallback) (WM_HWIN, int32_t);
	void (*releaseCallback) (WM_HWIN, int32_t);
	int32_t callbackParameter;
	int guiID;
} guiElement_t;

guiElement_t guiElement[20];

#define FREQUENCY_MAX 100000
#define FREQUENCY_MIN 10

volatile int32_t frequency = 1000;
volatile enum { SPEEDx1, SPEEDx2, SPEEDx4, SPEEDx8 } speedMode = SPEEDx1;

#define FREQUENCY_ID 100
#define AMPLITUDE_ID 101
#define OFFSET_ID 102

void updateWaveform(void);

void changeFrequency(WM_HWIN hWin, int32_t value)
{
	char tmp[50];
	WM_HWIN hItem;
	if (((frequency + value) <= FREQUENCY_MAX)
	    && ((frequency + value) >= FREQUENCY_MIN)) {
		frequency += value;
	}
	sprintf(tmp, "Frequency: %d Hz", frequency);

	if (frequency < 15000) {
		TIM6_PeriodSet((SystemCoreClock / 256) / frequency / 2);
		speedMode = SPEEDx1;
	} else if (frequency < 30000) {
		TIM6_PeriodSet((SystemCoreClock / 128) / frequency / 2);
		speedMode = SPEEDx2;
	} else if (frequency < 60000) {
		TIM6_PeriodSet((SystemCoreClock / 64) / frequency / 2);
		speedMode = SPEEDx4;
	} else {
		TIM6_PeriodSet((SystemCoreClock / 32) / frequency / 2);
		speedMode = SPEEDx8;
	}

	updateWaveform();

	hItem = WM_GetDialogItem(hWin, FREQUENCY_ID);
	TEXT_SetText(hItem, tmp);
}

const char waveformNames[4][20] = {
	"SINE",
	"SAWTOOTH",
	"TRIANGLE",
	"SQUARE"
};

uint8_t waveformMemory[256] = {	//SINE
	127, 130, 133, 136, 140, 143, 146, 149, 152, 155, 158, 161, 164,
	    167, 170, 173,
	176, 179, 182, 185, 187, 190, 193, 195, 198, 201, 203, 206, 208,
	    211, 213, 215,
	217, 220, 222, 224, 226, 228, 230, 232, 233, 235, 237, 238, 240,
	    241, 242, 244,
	245, 246, 247, 248, 249, 250, 251, 252, 252, 253, 253, 254, 254,
	    254, 254, 254,
	254, 254, 254, 254, 254, 253, 253, 252, 252, 251, 250, 250, 249,
	    248, 247, 246,
	244, 243, 242, 240, 239, 237, 236, 234, 232, 231, 229, 227, 225,
	    223, 221, 219,
	216, 214, 212, 209, 207, 204, 202, 199, 197, 194, 191, 189, 186,
	    183, 180, 177,
	175, 172, 169, 166, 163, 160, 157, 154, 150, 147, 144, 141, 138,
	    135, 132, 129,
	125, 122, 119, 116, 113, 110, 107, 104, 100, 97, 94, 91, 88, 85,
	    82, 79, 77, 74,
	71, 68, 65, 63, 60, 57, 55, 52, 50, 47, 45, 42, 40, 38, 35, 33, 31,
	    29, 27, 25, 23,
	22, 20, 18, 17, 15, 14, 12, 11, 10, 8, 7, 6, 5, 4, 4, 3, 2, 2, 1,
	    1, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 1, 1, 2, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 16, 17,
	    19, 21, 22, 24, 26, 28,
	30, 32, 34, 37, 39, 41, 43, 46, 48, 51, 53, 56, 59, 61, 64, 67, 69,
	    72, 75, 78, 81,
	84, 87, 90, 93, 96, 99, 102, 105, 108, 111, 114, 118, 121, 124, 127
};

const uint8_t waveforms[4][256] = {
	{			//SINE
	 127, 130, 133, 136, 140, 143, 146, 149, 152, 155, 158, 161, 164,
	 167, 170, 173,
	 176, 179, 182, 185, 187, 190, 193, 195, 198, 201, 203, 206, 208,
	 211, 213, 215,
	 217, 220, 222, 224, 226, 228, 230, 232, 233, 235, 237, 238, 240,
	 241, 242, 244,
	 245, 246, 247, 248, 249, 250, 251, 252, 252, 253, 253, 254, 254,
	 254, 254, 254,
	 254, 254, 254, 254, 254, 253, 253, 252, 252, 251, 250, 250, 249,
	 248, 247, 246,
	 244, 243, 242, 240, 239, 237, 236, 234, 232, 231, 229, 227, 225,
	 223, 221, 219,
	 216, 214, 212, 209, 207, 204, 202, 199, 197, 194, 191, 189, 186,
	 183, 180, 177,
	 175, 172, 169, 166, 163, 160, 157, 154, 150, 147, 144, 141, 138,
	 135, 132, 129,
	 125, 122, 119, 116, 113, 110, 107, 104, 100, 97, 94, 91, 88, 85,
	 82, 79, 77, 74,
	 71, 68, 65, 63, 60, 57, 55, 52, 50, 47, 45, 42, 40, 38, 35, 33,
	 31, 29, 27, 25, 23,
	 22, 20, 18, 17, 15, 14, 12, 11, 10, 8, 7, 6, 5, 4, 4, 3, 2, 2, 1,
	 1, 0, 0, 0, 0, 0, 0, 0,
	 0, 0, 0, 1, 1, 2, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 16, 17,
	 19, 21, 22, 24, 26, 28,
	 30, 32, 34, 37, 39, 41, 43, 46, 48, 51, 53, 56, 59, 61, 64, 67,
	 69, 72, 75, 78, 81,
	 84, 87, 90, 93, 96, 99, 102, 105, 108, 111, 114, 118, 121, 124,
	 127},

	{			//SAW
	 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
	 19, 20, 21, 22, 23, 24,
	 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
	 41, 42, 43, 44, 45, 46,
	 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62,
	 63, 64, 65, 66, 67, 68,
	 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84,
	 85, 86, 87, 88, 89, 90,
	 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105,
	 106, 107, 108, 109,
	 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122,
	 123, 124, 125,
	 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138,
	 139, 140, 141,
	 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154,
	 155, 156, 157,
	 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170,
	 171, 172, 173,
	 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186,
	 187, 188, 189,
	 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202,
	 203, 204, 205,
	 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218,
	 219, 220, 221,
	 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234,
	 235, 236, 237,
	 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250,
	 251, 252, 253,
	 254, 255},

	{			//TRIANGLE
	 255, 254, 252, 250, 248, 246, 244, 242, 240, 238, 236, 234, 232,
	 230, 228, 226,
	 224, 222, 220, 218, 216, 214, 212, 210, 208, 206, 204, 202, 200,
	 198, 196, 194,
	 192, 190, 188, 186, 184, 182, 180, 178, 176, 174, 172, 170, 168,
	 166, 164, 162,
	 160, 158, 156, 154, 152, 150, 148, 146, 144, 142, 140, 138, 136,
	 134, 132, 130,
	 128, 126, 124, 122, 120, 118, 116, 114, 112, 110, 108, 106, 104,
	 102, 100, 98,
	 96, 94, 92, 90, 88, 86, 84, 82, 80, 78, 76, 74, 72, 70, 68, 66,
	 64, 62, 60, 58, 56,
	 54, 52, 50, 48, 46, 44, 42, 40, 38, 36, 34, 32, 30, 28, 26, 24,
	 22, 20, 18, 16, 14,
	 12, 10, 8, 6, 4, 2, 0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24,
	 26, 28, 30, 32, 34,
	 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 64, 66,
	 68, 70, 72, 74, 76,
	 78, 80, 82, 84, 86, 88, 90, 92, 94, 96, 98, 100, 102, 104, 106,
	 108, 110, 112, 114,
	 116, 118, 120, 122, 124, 126, 128, 130, 132, 134, 136, 138, 140,
	 142, 144, 146,
	 148, 150, 152, 154, 156, 158, 160, 162, 164, 166, 168, 170, 172,
	 174, 176, 178,
	 180, 182, 184, 186, 188, 190, 192, 194, 196, 198, 200, 202, 204,
	 206, 208, 210,
	 212, 214, 216, 218, 220, 222, 224, 226, 228, 230, 232, 234, 236,
	 238, 240, 242,
	 244, 246, 248, 250, 252, 254},

	{			//SQUARE
	 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	 255, 255, 255,
	 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	 255, 255, 255,
	 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	 255, 255, 255,
	 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	 255, 255, 255,
	 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	 255, 255, 255,
	 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	 255, 255, 255,
	 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	 255, 255, 255,
	 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
	 255, 255, 255}

};


volatile enum { SIN = 0, SAW, TRI, SQ } waveform = SIN;
volatile int amplitude = 100;
volatile int offset = 0;

void updateWaveform(void)
{
	for (int i = 0; i < 256; i++) {
		int tmp;
		switch (speedMode) {
		case SPEEDx1:
			tmp = waveforms[waveform][i];
			break;
		case SPEEDx2:
			tmp = waveforms[waveform][(i * 2) % 255];
			break;
		case SPEEDx4:
			tmp = waveforms[waveform][(i * 4) % 255];
			break;
		case SPEEDx8:
			tmp = waveforms[waveform][(i * 8) % 255];
			break;
		}


		tmp = ((tmp - 127) * amplitude) / 100 + 127;

		tmp = tmp + offset;

		if (tmp > 255)
			tmp = 255;
		if (tmp < 0)
			tmp = 0;

		waveformMemory[i] = tmp;
	}
}

void changeWaveform(WM_HWIN hWin, int32_t value)
{
	WM_HWIN hItem;

	if (waveform < SQ) {
		waveform++;
	} else {
		waveform = SIN;
	}

	updateWaveform();

	hItem =
	    WM_GetDialogItem(hWin, guiElement[guiElementCount - 1].guiID);
	BUTTON_SetText(hItem, waveformNames[waveform]);
}


DAC_HandleTypeDef DacHandle;
DAC_ChannelConfTypeDef sConfig;

static void DialogCallback(WM_MESSAGE * pMsg)
{
	WM_HWIN hItem;
	int NCode;
	int Id;
	switch (pMsg->MsgId) {
	case WM_INIT_DIALOG:
//
// Initialization of 'Window'
//
		hItem = pMsg->hWin;
		WINDOW_SetBkColor(hItem, GUI_WHITE);
//
// Initialization of 'Image'
//
// in other place
		break;
	case WM_NOTIFY_PARENT:
		Id = WM_GetId(pMsg->hWinSrc);
		NCode = pMsg->Data.v;
		for (int i = 0; i < guiElementCount; i++) {	//check which element is called
			if (guiElement[i].guiID == Id) {
				switch (NCode) {
				case WM_NOTIFICATION_CLICKED:
					if (guiElement[i].clickCallback !=
					    NULL) {
						guiElement[i].
						    clickCallback(pMsg->
								  hWin,
								  guiElement
								  [i].
								  callbackParameter);
					}
					break;
				case WM_NOTIFICATION_RELEASED:
				case WM_NOTIFICATION_MOVED_OUT:
					if (guiElement[i].
					    releaseCallback != NULL) {
						guiElement[i].
						    releaseCallback(pMsg->
								    hWin,
								    guiElement
								    [i].
								    callbackParameter);
					}
					break;
				default:
					break;
				}
			}
		}
		//sliders
		if (NCode == WM_NOTIFICATION_VALUE_CHANGED) {
			hItem = WM_GetDialogItem(pMsg->hWin, Id);
			int value = SLIDER_GetValue(hItem);
			if (Id == AMPLITUDE_ID) {
				amplitude = value;
			} else if (Id == OFFSET_ID) {
				offset = value;
			}
			updateWaveform();
		}
		break;
	default:
		WM_DefaultProc(pMsg);
		break;
	}
}

void gen_gui_init(void)
{
	WM_HWIN hWin, hItem;

	GUI_Init();
	WM_SetCreateFlags(WM_CF_MEMDEV);

	BUTTON_SetDefaultSkin(BUTTON_SKIN_FLEX);
	FRAMEWIN_SetDefaultSkin(FRAMEWIN_SKIN_FLEX);

	// Ustawienie koloru tla pulpitu
	WM_SetDesktopColor(GUI_WHITE);

	DialogCreateList[0].pfCreateIndirect = WINDOW_CreateIndirect;
	DialogCreateList[0].Id = 0;
	DialogCreateList[0].x0 = 0;
	DialogCreateList[0].y0 = 0;
	DialogCreateList[0].xSize = 320;
	DialogCreateList[0].ySize = 240;
	DialogCreateList[0].Flags = 0;
	DialogCreateList[0].Para = 0;
	DialogCreateList[0].NumExtraBytes = 0;

	DialogElementCount = 1;

	guiElement[guiElementCount].x0 = 10;
	guiElement[guiElementCount].y0 = 10;
	guiElement[guiElementCount].xSize = 50;
	guiElement[guiElementCount].ySize = 30;
	guiElement[guiElementCount].text = (char *) malloc(6);
	strcpy(guiElement[guiElementCount].text, "+10k");
	guiElement[guiElementCount].clickCallback = changeFrequency;
	guiElement[guiElementCount].releaseCallback = NULL;
	guiElement[guiElementCount].callbackParameter = 10e3;

	guiElementCount++;

	guiElement[guiElementCount].x0 = 10;
	guiElement[guiElementCount].y0 = 80;
	guiElement[guiElementCount].xSize = 50;
	guiElement[guiElementCount].ySize = 30;
	guiElement[guiElementCount].text = (char *) malloc(6);
	strcpy(guiElement[guiElementCount].text, "-10k");
	guiElement[guiElementCount].clickCallback = changeFrequency;
	guiElement[guiElementCount].releaseCallback = NULL;
	guiElement[guiElementCount].callbackParameter = -10e3;

	guiElementCount++;

	guiElement[guiElementCount].x0 = 70;
	guiElement[guiElementCount].y0 = 10;
	guiElement[guiElementCount].xSize = 50;
	guiElement[guiElementCount].ySize = 30;
	guiElement[guiElementCount].text = (char *) malloc(6);
	strcpy(guiElement[guiElementCount].text, "+1k");
	guiElement[guiElementCount].clickCallback = changeFrequency;
	guiElement[guiElementCount].releaseCallback = NULL;
	guiElement[guiElementCount].callbackParameter = 1e3;

	guiElementCount++;

	guiElement[guiElementCount].x0 = 70;
	guiElement[guiElementCount].y0 = 80;
	guiElement[guiElementCount].xSize = 50;
	guiElement[guiElementCount].ySize = 30;
	guiElement[guiElementCount].text = (char *) malloc(6);
	strcpy(guiElement[guiElementCount].text, "-1k");
	guiElement[guiElementCount].clickCallback = changeFrequency;
	guiElement[guiElementCount].releaseCallback = NULL;
	guiElement[guiElementCount].callbackParameter = -1e3;

	guiElementCount++;

	guiElement[guiElementCount].x0 = 130;
	guiElement[guiElementCount].y0 = 10;
	guiElement[guiElementCount].xSize = 50;
	guiElement[guiElementCount].ySize = 30;
	guiElement[guiElementCount].text = (char *) malloc(6);
	strcpy(guiElement[guiElementCount].text, "+100");
	guiElement[guiElementCount].clickCallback = changeFrequency;
	guiElement[guiElementCount].releaseCallback = NULL;
	guiElement[guiElementCount].callbackParameter = 100;

	guiElementCount++;

	guiElement[guiElementCount].x0 = 130;
	guiElement[guiElementCount].y0 = 80;
	guiElement[guiElementCount].xSize = 50;
	guiElement[guiElementCount].ySize = 30;
	guiElement[guiElementCount].text = (char *) malloc(6);
	strcpy(guiElement[guiElementCount].text, "-100");
	guiElement[guiElementCount].clickCallback = changeFrequency;
	guiElement[guiElementCount].releaseCallback = NULL;
	guiElement[guiElementCount].callbackParameter = -100;

	guiElementCount++;

	guiElement[guiElementCount].x0 = 190;
	guiElement[guiElementCount].y0 = 10;
	guiElement[guiElementCount].xSize = 50;
	guiElement[guiElementCount].ySize = 30;
	guiElement[guiElementCount].text = (char *) malloc(6);
	strcpy(guiElement[guiElementCount].text, "+10");
	guiElement[guiElementCount].clickCallback = changeFrequency;
	guiElement[guiElementCount].releaseCallback = NULL;
	guiElement[guiElementCount].callbackParameter = 10;

	guiElementCount++;

	guiElement[guiElementCount].x0 = 190;
	guiElement[guiElementCount].y0 = 80;
	guiElement[guiElementCount].xSize = 50;
	guiElement[guiElementCount].ySize = 30;
	guiElement[guiElementCount].text = (char *) malloc(6);
	strcpy(guiElement[guiElementCount].text, "-10");
	guiElement[guiElementCount].clickCallback = changeFrequency;
	guiElement[guiElementCount].releaseCallback = NULL;
	guiElement[guiElementCount].callbackParameter = -10;

	guiElementCount++;

	guiElement[guiElementCount].x0 = 250;
	guiElement[guiElementCount].y0 = 10;
	guiElement[guiElementCount].xSize = 50;
	guiElement[guiElementCount].ySize = 30;
	guiElement[guiElementCount].text = (char *) malloc(6);
	strcpy(guiElement[guiElementCount].text, "+1");
	guiElement[guiElementCount].clickCallback = changeFrequency;
	guiElement[guiElementCount].releaseCallback = NULL;
	guiElement[guiElementCount].callbackParameter = 1;

	guiElementCount++;

	guiElement[guiElementCount].x0 = 250;
	guiElement[guiElementCount].y0 = 80;
	guiElement[guiElementCount].xSize = 50;
	guiElement[guiElementCount].ySize = 30;
	guiElement[guiElementCount].text = (char *) malloc(6);
	strcpy(guiElement[guiElementCount].text, "-1");
	guiElement[guiElementCount].clickCallback = changeFrequency;
	guiElement[guiElementCount].releaseCallback = NULL;
	guiElement[guiElementCount].callbackParameter = -1;

	guiElementCount++;

	guiElement[guiElementCount].x0 = 2;
	guiElement[guiElementCount].y0 = 198;
	guiElement[guiElementCount].xSize = 317;
	;
	guiElement[guiElementCount].ySize = 40;
	guiElement[guiElementCount].text = (char *) malloc(10);
	strcpy(guiElement[guiElementCount].text, waveformNames[waveform]);
	guiElement[guiElementCount].clickCallback = changeWaveform;
	guiElement[guiElementCount].releaseCallback = NULL;
	guiElement[guiElementCount].callbackParameter = 0;

	guiElementCount++;


	for (int i = 0; i < guiElementCount; i++) {
		DialogCreateList[DialogElementCount].pfCreateIndirect =
		    BUTTON_CreateIndirect;
		DialogCreateList[DialogElementCount].Id =
		    DialogElementCount;
		DialogCreateList[DialogElementCount].x0 = guiElement[i].x0;
		DialogCreateList[DialogElementCount].y0 = guiElement[i].y0;
		DialogCreateList[DialogElementCount].xSize =
		    guiElement[i].xSize;
		DialogCreateList[DialogElementCount].ySize =
		    guiElement[i].ySize;
		DialogCreateList[DialogElementCount].Flags = 0;
		DialogCreateList[DialogElementCount].Para = 0;
		DialogCreateList[DialogElementCount].NumExtraBytes = 0;

		guiElement[i].guiID = DialogElementCount;

		DialogElementCount++;
	}



	DialogCreateList[DialogElementCount].pfCreateIndirect =
	    TEXT_CreateIndirect;
	DialogCreateList[DialogElementCount].Id = FREQUENCY_ID;
	DialogCreateList[DialogElementCount].x0 = 2;
	DialogCreateList[DialogElementCount].y0 = 40;
	DialogCreateList[DialogElementCount].xSize = 318;
	DialogCreateList[DialogElementCount].ySize = 40;
	DialogCreateList[DialogElementCount].Flags =
	    TEXT_CF_LEFT | TEXT_CF_VCENTER;
	DialogCreateList[DialogElementCount].Para = 0;
	DialogCreateList[DialogElementCount].NumExtraBytes = 0;

	DialogElementCount++;

	DialogCreateList[DialogElementCount].pfCreateIndirect =
	    SLIDER_CreateIndirect;
	DialogCreateList[DialogElementCount].Id = AMPLITUDE_ID;
	DialogCreateList[DialogElementCount].x0 = 52;
	DialogCreateList[DialogElementCount].y0 = 120;
	DialogCreateList[DialogElementCount].xSize = 268;
	DialogCreateList[DialogElementCount].ySize = 30;
	DialogCreateList[DialogElementCount].Flags = 0;
	DialogCreateList[DialogElementCount].Para = 0;
	DialogCreateList[DialogElementCount].NumExtraBytes = 0;

	DialogElementCount++;

	DialogCreateList[DialogElementCount].pfCreateIndirect =
	    TEXT_CreateIndirect;
	DialogCreateList[DialogElementCount].Id = AMPLITUDE_ID + 100;
	DialogCreateList[DialogElementCount].x0 = 2;
	DialogCreateList[DialogElementCount].y0 = 120;
	DialogCreateList[DialogElementCount].xSize = 50;
	DialogCreateList[DialogElementCount].ySize = 30;
	DialogCreateList[DialogElementCount].Flags =
	    TEXT_CF_HCENTER | TEXT_CF_VCENTER;
	DialogCreateList[DialogElementCount].Para = 0;
	DialogCreateList[DialogElementCount].NumExtraBytes = 0;

	DialogElementCount++;

	DialogCreateList[DialogElementCount].pfCreateIndirect =
	    SLIDER_CreateIndirect;
	DialogCreateList[DialogElementCount].Id = OFFSET_ID;
	DialogCreateList[DialogElementCount].x0 = 52;
	DialogCreateList[DialogElementCount].y0 = 150;
	DialogCreateList[DialogElementCount].xSize = 268;
	DialogCreateList[DialogElementCount].ySize = 30;
	DialogCreateList[DialogElementCount].Flags = 0;
	DialogCreateList[DialogElementCount].Para = 0;
	DialogCreateList[DialogElementCount].NumExtraBytes = 0;

	DialogElementCount++;

	DialogCreateList[DialogElementCount].pfCreateIndirect =
	    TEXT_CreateIndirect;
	DialogCreateList[DialogElementCount].Id = OFFSET_ID + 100;
	DialogCreateList[DialogElementCount].x0 = 2;
	DialogCreateList[DialogElementCount].y0 = 150;
	DialogCreateList[DialogElementCount].xSize = 50;
	DialogCreateList[DialogElementCount].ySize = 30;
	DialogCreateList[DialogElementCount].Flags =
	    TEXT_CF_HCENTER | TEXT_CF_VCENTER;
	DialogCreateList[DialogElementCount].Para = 0;
	DialogCreateList[DialogElementCount].NumExtraBytes = 0;

	DialogElementCount++;


	hWin =
	    GUI_CreateDialogBox(DialogCreateList, DialogElementCount,
				DialogCallback, WM_HBKWIN, 0, 0);

	//waveform button
	hItem =
	    WM_GetDialogItem(hWin, guiElement[guiElementCount - 1].guiID);
	BUTTON_SetFont(hItem, &GUI_Font8x16x2x2);

	//sliders descriptions
	hItem = WM_GetDialogItem(hWin, AMPLITUDE_ID + 100);
	TEXT_SetFont(hItem, &GUI_Font8x16x2x2);
	TEXT_SetText(hItem, "A:");

	hItem = WM_GetDialogItem(hWin, OFFSET_ID + 100);
	TEXT_SetFont(hItem, &GUI_Font8x16x2x2);
	TEXT_SetText(hItem, "O:");

	for (int i = 0; i < guiElementCount; i++) {
		hItem = WM_GetDialogItem(hWin, guiElement[i].guiID);
		BUTTON_SetText(hItem, guiElement[i].text);
	}

	hItem = WM_GetDialogItem(hWin, AMPLITUDE_ID);
	SLIDER_SetValue(hItem, amplitude);

	hItem = WM_GetDialogItem(hWin, OFFSET_ID);
	SLIDER_SetValue(hItem, offset);
	SLIDER_SetRange(hItem, -127, 127);

	//frequency display
	hItem = WM_GetDialogItem(hWin, FREQUENCY_ID);
	TEXT_SetFont(hItem, &GUI_Font8x16x2x2);
	changeFrequency(hWin, 0);
}

/*********************************************************************
*
*       Main
*/
int main(void)
{
	HAL_Init();		/* Initialize the HAL Library */
	SystemClock_Config();	/* Configure the System Clock */

	DacHandle.Instance = DAC;
	sConfig.DAC_Trigger = DAC_TRIGGER_T6_TRGO;
	sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;

	gen_gui_init();

	TIM6_Config();

	TIM6_PeriodSet((SystemCoreClock / 256) / frequency / 2);

	HAL_DAC_DeInit(&DacHandle);

	HAL_DAC_Init(&DacHandle);

	HAL_DAC_ConfigChannel(&DacHandle, &sConfig, DACx_CHANNEL1);

	HAL_DAC_Start_DMA(&DacHandle, DACx_CHANNEL1,
			  (uint32_t *) waveformMemory, 256,
			  DAC_ALIGN_8B_R);

	while (1) {
		GUI_Exec();
	}
}

/*************************** End of file ****************************/
