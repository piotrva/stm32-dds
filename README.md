# STM32-DDS

A simple, minimalistic STM32F492I DDS (STM32F429I-DISCO / STM32F429I-DISC1) with GUI on LCD touchscreen.

Output on PA5.

Requires Board Support for STM32F429I-DISCO / STM32F429I-DISC1 installed using Keil tools
