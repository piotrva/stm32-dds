#ifndef _SYSTEM_H_
#define _SYSTEM_H_

#include <inttypes.h>

void SystemClock_Config(void);
void TIM6_Config(void);
void TIM6_PeriodSet(uint32_t Period);

#endif
